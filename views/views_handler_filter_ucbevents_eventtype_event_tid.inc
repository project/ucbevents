<?php

/**
 * @file
 * Views filter by event type (tid).
 */
class views_handler_filter_ucbevents_eventtype_event_tid extends views_handler_filter_many_to_one {
  function get_value_options() {
    if (isset($this->value_options)) {
      return;
    }

    $this->value_options = array();

    $result = db_query('SELECT tid, name FROM {ucbevents_eventtype} ORDER BY name');
    foreach ($result as $eventtype) {
      $this->value_options[$eventtype->tid] = $eventtype->name;
    }
  }
}

<?php

/**
 * @file
 * Views filter by event feed (fid).
 */
class views_handler_filter_ucbevents_event_feed_fid extends views_handler_filter_many_to_one {
  function get_value_options() {
    if (isset($this->value_options)) {
      return;
    }

    $this->value_options = array();

    $result = db_query('SELECT fid, title FROM {ucbevents_feed} ORDER BY title');
    foreach ($result as $feed) {
      $this->value_options[$feed->fid] = $feed->title;
    }
  }
}

<?php

/**
 * @file
 * Views argument handler to accept an event id.
 */
class views_handler_argument_ucbevents_eid extends views_handler_argument_numeric {
  /**
   * Override the behavior of title(). Get the title of the event.
   */
  function title_query() {
    $titles = array();
    $result = db_query("SELECT title FROM {ucbevents_event} WHERE eid IN (:eids)", array(':eids' => $this->value));
    foreach ($result as $term) {
      $titles[] = check_plain($term->title);
    }
    return $titles;
  }
}

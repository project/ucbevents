<?php

/**
 * @file
 * Views field handler for the event title.
 */
/**
 * Turns an event's title into a clickable link to the original event posting.
 */
class views_handler_field_ucbevents_title_link extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['eid'] = 'eid';
    $this->additional_fields['subtitle'] = 'subtitle';
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['display_as_link'] = array('default' => TRUE);
    $options['ucbevents_link'] = array('default' => 'http://events.berkeley.edu/?event_ID=[eid]');
    $options['display_subtitle'] = array('default' => FALSE);

    return $options;
  }

  /**
   * Provide link to the page being visited.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['display_as_link'] = array(
      '#title' => t('Display as link'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['display_as_link']),
    );
    $form['ucbevents_link'] = array(
      '#title' => t('UCB Events URL'),
      '#type' => 'textfield',
      '#default_value' => $this->options['ucbevents_link'],
      '#description' => t('The [eid] token will be replaced with the numeric event id.'),
      '#process' => array('ctools_dependent_process'),
      '#dependency' => array(
        'edit-options-display-as-link' => array(1),
      ),
    );
    $form['display_subtitle'] = array(
      '#title' => t('Display subtitle'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['display_subtitle']),
    );
  }

  function render($values) {
    $value = $values->{$this->field_alias};
    if (!empty($this->options['display_subtitle'])) {
      // Append the subtitle, if present.
      $subtitle = $values->{$this->aliases['subtitle']};
      if (!empty($subtitle)) {
        $value .= ': ' . $subtitle;
      }
    }
    if (!empty($this->options['display_as_link'])) {
      $tokens = array('[eid]' => $values->{$this->aliases['eid']});
      $link = strtr($this->options['ucbevents_link'], $tokens);
      return l(check_plain($value), $link, array('html' => TRUE));
    }
    else {
      return check_plain($value);
    }
  }
}

<?php

/**
 * @file
 * Field handler to provide a list event feeds.
 */
class views_handler_field_ucbevents_event_feed_fid extends views_handler_field_prerender_list {
  function construct() {
    parent::construct();
    $this->additional_fields['eid'] = array(
      'table' => 'ucbevents_event',
      'field' => 'eid',
    );
  }

  function query() {
    $this->add_additional_fields();
    $this->field_alias = $this->aliases['eid'];
  }

  function pre_render($values) {
    $eids = array();
    $this->items = array();

    foreach ($values as $result) {
      $eids[] = $result->{$this->aliases['eid']};
    }

    if ($eids) {
      $query = db_select('ucbevents_feed', 'f');
      $query->join('ucbevents_event_feed', 'e', 'e.fid = f.fid');
      $query->fields('e', array('eid', 'fid'));
      $query->fields('f', array('title'));
      $query->condition('e.eid', $eids, 'IN');
      $query->orderBy('f.title');
      $result = $query->execute();
      foreach ($result as $feed) {
        $this->items[$feed->eid][$feed->fid]['title'] = check_plain($feed->title);
        $this->items[$feed->eid][$feed->fid]['fid'] = $feed->fid;
      }
    }
  }

  function render_item($count, $item) {
    return $item['title'];
  }

  function document_self_tokens(&$tokens) {
    $tokens['[' . $this->options['id'] . '-title' . ']'] = t('The title of the event feed.');
    $tokens['[' . $this->options['id'] . '-fid' . ']'] = t('The feed ID of the event feed.');
  }

  function add_self_tokens(&$tokens, $item) {
    $tokens['[' . $this->options['id'] . '-title' . ']'] = $item['title'];
    $tokens['[' . $this->options['id'] . '-fid' . ']'] = $item['fid'];
  }
}

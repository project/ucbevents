<?php

/**
 * @file
 * Views field handler for a virtual 'link' field.
 */

/**
 * Creates a link to events.berkeley.edu by prepending an editable prefix to
 * the event id.
 */
class views_handler_field_ucbevents_link extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['eid'] = 'eid';
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['display_as_link'] = array('default' => TRUE);
    $options['ucbevents_link'] = array('default' => 'http://events.berkeley.edu/?event_ID=[eid]');

    return $options;
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  /**
   * Provide link to the page being visited.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['display_as_link'] = array(
      '#title' => t('Display as link'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['display_as_link']),
    );
    $form['ucbevents_link'] = array(
      '#title' => t('UCB Events URL'),
      '#type' => 'textfield',
      '#default_value' => $this->options['ucbevents_link'],
      '#description' => t('The [eid] token will be replaced with the numeric event id.'),
    );
  }

  function render($values) {
    $tokens = array('[eid]' => $values->{$this->aliases['eid']});
    $link = strtr($this->options['ucbevents_link'], $tokens);

    if (!empty($this->options['display_as_link'])) {
      return l(check_plain($link), $link, array('html' => TRUE));
    }
    else {
      return check_url($link);
    }
  }
}

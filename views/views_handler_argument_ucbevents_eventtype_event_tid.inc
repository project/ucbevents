<?php

/**
 * @file
 * Views argument handler to accept an event type id.
 */
class views_handler_argument_ucbevents_eventtype_event_tid extends views_handler_argument_many_to_one {
  /**
   * Override the behavior of title(). Get the title of the category.
   */
  function title_query() {
    $titles = array();
    $result = db_query("SELECT name FROM {ucbevents_eventtype} WHERE tid IN (:tids)", array(':tids' => $this->value));
    foreach ($result as $term) {
      $titles[] = check_plain($term->name);
    }
    return $titles;
  }
}

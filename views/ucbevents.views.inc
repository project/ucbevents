<?php
/**
 * @file
 * Provide views data and handlers for ucbevents.module
 */

/**
 * @defgroup views_ucbevents_module ucbevents.module handlers
 *
 * Includes the 'ucbevents_event,' 'ucbevents_eventtype,' and 'ucbevents_feed'
 * tables.
 *
 * @{
 */

/**
 * Implements hook_views_data().
 */
function ucbevents_views_data() {
  // ----------------------------------------------------------------------
  // Main UCB Event base table

  // Define the base group of this table. Fields that don't
  // have a group defined will go into this field by default.
  $data['ucbevents_event']['table']['group']  = t('UCB Event');

  // Advertise this table as a possible base table
  $data['ucbevents_event']['table']['base'] = array(
    'field' => 'eid',
    'title' => t('UCB Event item'),
    'help' => t("UCB Event items are imported from events.berkeley.edu."),
  );

  // ----------------------------------------------------------------
  // Fields

  // eid
  $data['ucbevents_event']['eid'] = array(
    'title' => t('Event ID'),
    'help' => t('The unique ID of the ucbevents item.'), // The help that appears on the UI,
    // Information for displaying the eid
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    // Information for accepting a eid as an argument
    'argument' => array(
      'handler' => 'views_handler_argument_ucbevents_eid',
      'name field' => 'title', // the field to display in the summary.
      'numeric' => TRUE,
    ),
    // Information for accepting a nid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    // Information for sorting on a nid.
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // title
  $data['ucbevents_event']['title'] = array(
    'title' => t('Title'), // The item it appears as on the UI,
    'help' => t('The title of the ucbevents item.'),
    // Information for displaying a title as a field
    'field' => array(
      'handler' => 'views_handler_field_ucbevents_title_link',
      'extra' => array('eid', 'subtitle'),
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    // Information for accepting a title as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // subtitle
  $data['ucbevents_event']['subtitle'] = array(
    'title' => t('Subtitle'), // The item it appears as on the UI,
    'help' => t('The subtitle of the ucbevents item.'),
    // Information for displaying a title as a field
    'field' => array(
      'handler' => 'views_handler_field_xss',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    // Information for accepting a title as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // link
  $data['ucbevents_event']['link'] = array(
    'title' => t('Link'), // The item it appears as on the UI,
    'help' => t('The link to the events.berkeley.edu page of the event.'),
    'field' => array(
      'handler' => 'views_handler_field_ucbevents_link',
      'click sortable' => TRUE,
      'extra' => array('eid'),
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    // Information for accepting a title as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // description
  $data['ucbevents_event']['description'] = array(
    'title' => t('Short Description'), // The item it appears as on the UI,
    'help' => t('The actual content of the imported item.'),
    // Information for displaying a title as a field
    'field' => array(
      'handler' => 'views_handler_field_xss',
      'click sortable' => FALSE,
    ),
    // Information for accepting a title as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // item timestamp
  $data['ucbevents_event']['start_time'] = array(
    'title' => t('Start Time'), // The item it appears as on the UI,
    'help' => t('The starting time of the event'),
    // Information for displaying a title as a field
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    // Information for accepting a title as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_date',
    ),
  );


  // ----------------------------------------------------------------------
  // Event feed table

  $data['ucbevents_event_feed']['table']['group']  = t('UCB Event');

  $data['ucbevents_event_feed']['table']['join'] = array(
    'ucbevents_event' => array(
      'left_field' => 'eid',
      'field' => 'eid',
    ),
  );

  // fid
  $data['ucbevents_event_feed']['fid'] = array(
    'title' => t('Feed ID'),
    'help' => t('The unique ID of the ucbevents feed.'), // The help that appears on the UI,
    // Information for displaying the fid
    'field' => array(
      'handler' => 'views_handler_field_ucbevents_event_feed_fid',
      'click sortable' => TRUE,
    ),
    // Information for accepting a fid as an argument
    'argument' => array(
      'handler' => 'views_handler_argument_ucbevents_event_feed_fid',
      'name table' => 'ucbevents_feed',
      'name field' => 'title', // the field to display in the summary.
      'numeric' => TRUE,
    ),
    // Information for accepting a nid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_ucbevents_event_feed_fid',
    ),
    // Information for sorting on a fid.
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );


  // ----------------------------------------------------------------------
  // Event feed table

  // Explain how this table joins to others.
  $data['ucbevents_feed']['table']['join'] = array(
    'ucbevents_event' => array(
      'left_table' => 'ucbevents_event_feed',
      'left_field' => 'fid',
      'field' => 'fid',
    ),
  );

  // ----------------------------------------------------------------------
  // Events event type table

  $data['ucbevents_eventtype_event']['table']['group'] = t('UCB Event');

  $data['ucbevents_eventtype_event']['table']['join'] = array(
    'ucbevents_event' => array(
      'left_field' => 'eid',
      'field' => 'eid',
    ),
  );

  // tid
  $data['ucbevents_eventtype_event']['tid'] = array(
    'title' => t('Event Type'),
    'help' => t('The event type of the event.'),
    'field' => array(
      'handler' => 'views_handler_field_ucbevents_eventtype_event_tid',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_ucbevents_eventtype_event_tid',
      'name table' => 'ucbevents_eventtype',
      'name field' => 'name',
      'numeric' => TRUE,
      'empty field name' => t('No type'),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_ucbevents_eventtype_event_tid',
      'numeric' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // ----------------------------------------------------------------------
  // Events event type table
  $data['ucbevents_eventtype']['table']['join'] = array(
    // Directly links to users table.
    'ucbevents_event' => array(
      'left_table' => 'ucbevents_eventtype_event',
      'left_field' => 'tid',
      'field' => 'tid',
    ),
    // needed for many to one helper sometimes
    'ucbevents_eventtype_event' => array(
      'left_field' => 'tid',
      'field' => 'tid',
    ),
  );

  return $data;
}

/**
 * @}
 */

<?php

/**
 * @file
 * Field handler to provide a list event types.
 */
class views_handler_field_ucbevents_eventtype_event_tid extends views_handler_field_prerender_list {
  function construct() {
    parent::construct();
    $this->additional_fields['eid'] = array(
      'table' => 'ucbevents_event',
      'field' => 'eid',
    );
  }

  function query() {
    $this->add_additional_fields();
    $this->field_alias = $this->aliases['eid'];
  }

  function pre_render($values) {
    $eids = array();
    $this->items = array();

    foreach ($values as $result) {
      $eids[] = $result->{$this->aliases['eid']};
    }

    if ($eids) {
      $query = db_select('ucbevents_eventtype', 't');
      $query->join('ucbevents_eventtype_event', 'e', 'e.tid = t.tid');
      $query->fields('e', array('eid', 'tid'));
      $query->fields('t', array('name', 'type'));
      $query->condition('e.eid', $eids, 'IN');
      $query->orderBy('t.name');
      $result = $query->execute();
      foreach ($result as $eventtype) {
        $this->items[$eventtype->eid][$eventtype->tid]['name'] = check_plain($eventtype->name);
        $this->items[$eventtype->eid][$eventtype->tid]['type'] = check_plain($eventtype->type);
        $this->items[$eventtype->eid][$eventtype->tid]['tid'] = $eventtype->tid;
      }
    }
  }

  function render_item($count, $item) {
    return $item['name'];
  }

  function document_self_tokens(&$tokens) {
    $tokens['[' . $this->options['id'] . '-name' . ']'] = t('The name of the event type.');
    $tokens['[' . $this->options['id'] . '-type' . ']'] = t('The type of the event type. (Primary or Secondary).');
    $tokens['[' . $this->options['id'] . '-tid' . ']'] = t('The event type ID of the event type.');
  }

  function add_self_tokens(&$tokens, $item) {
    $tokens['[' . $this->options['id'] . '-name' . ']'] = $item['name'];
    $tokens['[' . $this->options['id'] . '-type' . ']'] = $item['type'];
    $tokens['[' . $this->options['id'] . '-tid' . ']'] = $item['tid'];
  }
}

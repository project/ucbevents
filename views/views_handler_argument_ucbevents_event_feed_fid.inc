<?php

/**
 * @file
 * Views argument handler to accept an event feed id.
 */
class views_handler_argument_ucbevents_event_feed_fid extends views_handler_argument_many_to_one {
  /**
   * Override the behavior of title(). Get the title of the category.
   */
  function title_query() {
    $titles = array();
    $result = db_query("SELECT title FROM {ucbevents_feed} WHERE fid IN (:fids)", array(':fids' => $this->value));
    foreach ($result as $term) {
      $titles[] = check_plain($term->title);
    }
    return $titles;
  }
}

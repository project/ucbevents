UC Berkeley Events
==================

A Drupal 7.x module for integration with the UC Berkeley Events Calendar.

Reads event data from the events.berkeley.edu XML (Live Export) data feeds.


Requirements
------------

The module has some modest requirements which are outlined below.

* [Drupal](http://www.drupal.org/project/drupal/) 7.x
* [Views](http://www.drupal.org/project/views/) 7.x-3.x

Installation
------------

The usual module installation methods apply.

There is no UI at the moment. Instead, feed retrieval is controlled via the
`{ucbevents_feeds}` table.

You can manually enter in the data into SQL, or use drush to execute a script:

    // Add a default first feed.
    $feed = array(
      'title' => $title,
      'url' => $url,
      'refresh' => 6*60*60, // refresh every six hours
    );
    drupal_write_record('ucbevents_feed', $feed);

The `$title` is descriptive and may be set to whatever you like.  The `$url`
should be of the form:

    http://events.berkeley.edu/index.php/live_export/sn/{your-short-name}/...

The actual feed specification will depend on your own needs. Available options
are outlined under the [Admin Help](http://events.berkeley.edu/index.php/admin_help.html#rss).


Authors
-------

* Bradley M. Froehle (bfroehle@berkeley.edu)

<?php

/**
 * @file
 * Hooks provided by the UCB Events module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * The event was built; the module may modify the structured content.
 *
 * This hook is called after the event has been assembled but before it is
 * saved.
 *
 * @param $event
 *   An associative array containing event data, with the following keys:
 *   - eid: numeric event id.
 *   - title: title of the event.
 *   - subtitle: subtitle of the event.
 *   - description: description of the event.
 *   - start_time: starting time of the event as a unix timestamp.
 *   - types: array keyed by event type id and values an associative array:
 *     - name: human readable name of the event type.
 *     - type: type of the event type (either Primary or Secondary).
 */
function hook_ucbevents_event_alter(&$event) {
  // Remove unneccessary text from the event title.
  $event['title'] = preg_replace('/^(Foo Bar Baz|FBB):? /i', '', $event['title']);
}

/**
 * The event type was built; the module may modify the structured content.
 *
 * This hook is called after the event type has been assembled but before it is
 * saved.
 *
 * @param $eventtype
 *   An associative array containing an event type, with the following keys:
 *   - name: human readable name of the event type.
 *   - type: type of the event type (either Primary or Secondary).
 */
function hook_ucbevents_eventtype_alter(&$eventtype) {
  // Customize the event type name.
  if ($eventtype['name'] == 'Foo') {
    $eventtype['name'] = 'Bar';
  }
}


 /**
  * @} End of "addtogroup hooks".
  */
